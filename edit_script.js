const gBASE_URL = "https://devcamp-student.herokuapp.com/users/";
var gUserId;

function onBtnActionClick(paramAction) {
  if (paramAction == 'back') {
    window.location.href = "index.html";
  } else {
    updateNewUser();
  }
}

function updateNewUser() {
  //B1: thu thập dữ liệu
  var vObjectRequestData = {
    name: $("#edit-fullname").val().trim(),
    email: $("#edit-email").val().trim(),
    password: "123456",
    phone: $("#edit-phone").val().trim(),
    birthday: $("#edit-birthday").val().trim(),
    gender: "Female",
  };

  //B2: kiểm tra dữ liệu
  var vCheck = validateData(vObjectRequestData);

  //B3: Xử lý dữ liệu
  if (vCheck) {
    callAjaxUpdateUser(vObjectRequestData);
  }
}

function validateData(paramObj) {
  var vResult = true;
  if (paramObj.name == "") {
    alert("Chưa nhập họ tên");
    vResult = false
  }
  if (paramObj.email == "") {
    alert("Chưa nhập email");
    vResult = false
  }
  if (!paramObj.email.includes("@")) {
    alert("Email ko có @");
    vResult = false
  }
  if (paramObj.phone == "") {
    alert("Chưa nhập phone");
    vResult = false
  }
  return vResult;
}

function callAjaxUpdateUser(paramObj) {
  $.ajax({
    url: gBASE_URL + gUserId,
    type: "PUT",
    dataType: "json",
    data: JSON.stringify(paramObj),
    contentType: "application/json;charset=UTF-8",
    async: false,
    success: function (data) {
      alert("Cập nhật thành công user có id: " + data.id);
      window.location.href = "index.html";
    }
  });
}

function onPageLoading() {
  //Load user detail
  const vQueryString = window.location.href;
  const vUrl = new URL(vQueryString);
  gUserId = vUrl.searchParams.get("id");
  console.log("Id là: " + gUserId);
  callApiGetUserById(gUserId);
}

function callApiGetUserById(paramUserId) {
  $.ajax({
    url: gBASE_URL + paramUserId,
    type: 'GET',
    dataType: 'json', // added data type
    async: false,
    success: function (res) {
      console.log(res);
      //Khi thành công, đổ data vào các trường input
      showUserDetail(res);
    },
    error: function (ajaxContext) {
      alert(ajaxContext.responseText)
    }
  });
}

function showUserDetail(paramUserObj) {
  $("#edit-fullname").val(paramUserObj.name);
  $("#edit-birthday").val(paramUserObj.birthday);
  $("#edit-email").val(paramUserObj.email);
  $("#edit-phone").val(paramUserObj.phone);
}

$(document).ready(function () {
  onPageLoading();

  $(".btn-back").on('click', function () {
    onBtnActionClick('back');
  });

  $(".btn-update").on('click', function () {
    onBtnActionClick('update');
  });
});