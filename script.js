//Global variable
const gBASE_URL = "https://devcamp-student.herokuapp.com/users/";
var gUserDatas;

function loadDataIntoTable(paramUserObj) {
  var vUserTable = $("#tbody-user");
  // Clear bảng trước khi load data
  vUserTable.empty();
  //Lấy ra dc array chứa thông tin user
  var vUserArray = paramUserObj;
  //Loop qua từng phần tử array
  $.each(vUserArray, function (index, value) {
    //Mỗi phần tử array là 1 object, lấy key, value của từng object đó
    var bUser = value;
    //Bỏ data vào table cho từng dòng thông tin users
    var vNewRow = $("<tr>").appendTo(vUserTable).append($("<td>", { html: bUser.name }));
    $("<td>", { html: bUser.birthday }).appendTo(vNewRow);
    $("<td>", { html: bUser.email }).appendTo(vNewRow);
    $("<td>", { html: bUser.phone }).appendTo(vNewRow);

    var bActionCell = $("<td>").appendTo(vNewRow);
    bActionCell.append('<a href="#" data-id="' + bUser.id + '" class="user-detail"><i class="fa fa-edit"></i>Chỉnh sửa</a>')
      .append(' <a href="#" data-id="' + bUser.id + '" class="user-delete text-danger"><i class="fa fa-trash-alt"></i>Xoá</a>')
  });
}

function getAllUsers() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    dataType: "json",
    async: false,
    success: function (data) {
      gUserDatas = data;
      loadDataIntoTable(gUserDatas);
    }
  });
}


function onPageLoading() {
  getAllUsers();
}

function onBtnActionClick(paramData, paramAction) {
  console.log("Nút " + paramAction + " được ấn");
  var vUserId = $(paramData).data("id");
  console.log("User Id: " + vUserId);
  if (paramAction == 'detail') {
    const vDETAIL_USER_URL = "edit_user.html";
    var vUrlSiteOpen = vDETAIL_USER_URL + "?id=" + vUserId;
    window.location.href = vUrlSiteOpen;
  }
}

$(document).ready(function () {
  onPageLoading();

  $(".user-detail").on('click', function () {
    onBtnActionClick(this, 'detail');
  });

  $(".user-delete").on('click', function () {
    onBtnActionClick(this, 'delete');
  });

  $(".btn-add-user").on('click', function () {
    window.location.href = "add_new_user.html";
  });
});